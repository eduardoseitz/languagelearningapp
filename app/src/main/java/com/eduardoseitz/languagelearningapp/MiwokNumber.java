package com.eduardoseitz.languagelearningapp;

import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class MiwokNumber extends AppCompatActivity {

    /** Handles playback of all the sound files */
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miwok_number);

        // Create wordList
        final ArrayList<Word> wordList = new ArrayList<Word>();

        // Add list elements
        wordList.add(new Word("Lutti", "One", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_one));
        wordList.add(new Word("Otiiko", "Two", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_two));
        wordList.add(new Word("Tolookusu", "Three", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_three));
        wordList.add(new Word("Oyyisa", "Four", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_four));
        wordList.add(new Word("Massoka", "Five", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_five));
        wordList.add(new Word("Temmokka", "Six ", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_six));
        wordList.add(new Word("Kenekaku", "Seven", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_seven));
        wordList.add(new Word("Kawinta", "Eight", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_eight));
        wordList.add(new Word("Wo’e", "Nine", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_nine));
        wordList.add(new Word("Na’aacha", "Ten", R.drawable.numeric_white, R.color.category_numbers, R.raw.number_ten));

        // Recycle list items with a list view
        final WordArrayAdapter wordArrayAdapter = new WordArrayAdapter(this, wordList);

        ListView listView = findViewById(R.id.word_list_view);
        listView.setAdapter(wordArrayAdapter);

        // Play sound
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                // Create and setup the {@link MediaPlayer} for the audio resource associated
                // with the current word
                mediaPlayer = MediaPlayer.create(MiwokNumber.this, wordList.get(position).GetSound());

                // Start the audio file
                mediaPlayer.start();
            }
        });

        // Link layout elements
        /*
        LinearLayout rootLinearLayout = findViewById(R.id.color_category_root_linear_layout);

        for (int word = 0; word < wordList.size(); word++)
        {
            // Create elements
            LinearLayout wordLayout = new LinearLayout(this, null, R.style.WordListStyle);
            ImageView wordIcon = new ImageView(this, null, R.style.WordIconStyle);
            TextView wordText = new TextView(this, null, R.style.WordTextStyle);

            // Assign elements parents
            rootLinearLayout.addView(wordLayout);
            wordLayout.addView(wordIcon);
            wordLayout.addView(wordText);

            // Edit elements parameters
            wordIcon.setBackgroundResource(R.drawable.palette);
            wordText.setText(wordList.get(word));
        }
        */
    }
}
