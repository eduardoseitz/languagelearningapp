package com.eduardoseitz.languagelearningapp;

public class Word
{
    private String _original;
    private String _translation;
    private int _icon;
    private int _color;
    private int _sound;

    public Word(String originalWord, String translatedWord, int wordIcon, int wordColor, int wordSound){
        _original = originalWord;
        _translation = translatedWord;
        _icon = wordIcon;
        _color = wordColor;
        _sound = wordSound;
    }

    public String GetOriginal()
    {
        return _original;
    }

    public String GetTranslation()
    {
        return _translation;
    }

    public int GetIcon(){return _icon; }

    public int GetColor(){return _color; }

    public int GetSound(){return _sound; }
}
