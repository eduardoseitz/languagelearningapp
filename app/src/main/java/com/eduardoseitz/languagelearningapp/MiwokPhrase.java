package com.eduardoseitz.languagelearningapp;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MiwokPhrase extends AppCompatActivity {

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miwok_phrase);

        // Create wordList
        final ArrayList<Word> wordList = new ArrayList<Word>();

        // Add list elements
        wordList.add(new Word("minto wuksus", "Where are you going?", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_where_are_you_going));
        wordList.add(new Word("tinnә oyaase'nә", "What is your name?", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_what_is_your_name));
        wordList.add(new Word("oyaaset..", "My name is...", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_my_name_is));
        wordList.add(new Word("michәksәs?", "How are you feeling?", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_how_are_you_feeling));
        wordList.add(new Word("kuchi achit", "I’m feeling good", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_im_feeling_good));
        wordList.add(new Word("әәnәs'aa?", "Are you coming? ", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_are_you_coming));
        wordList.add(new Word("hәә’ әәnәm", "Yes, I’m coming.", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_yes_im_coming));
        wordList.add(new Word("әәnәm", "I’m coming.", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_im_coming));
        wordList.add(new Word("yoowutis", "Let’s go.", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_lets_go));
        wordList.add(new Word("әnni'nem", "Come here", R.drawable.format_letter_case_white, R.color.category_phrases, R.raw.phrase_come_here));

        // Recycle list items with a list view
        WordArrayAdapter wordArrayAdapter = new WordArrayAdapter(this, wordList);

        ListView listView = findViewById(R.id.word_list_view);
        listView.setAdapter(wordArrayAdapter);

        // Play sound
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                // Create and setup the {@link MediaPlayer} for the audio resource associated
                // with the current word
                mediaPlayer = MediaPlayer.create(MiwokPhrase.this, wordList.get(position).GetSound());

                // Start the audio file
                mediaPlayer.start();
            }
        });
    }
}
