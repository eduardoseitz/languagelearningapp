package com.eduardoseitz.languagelearningapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_main.xml layout file
        setContentView(R.layout.activity_main);

        final LinearLayout colorCategoryButton = findViewById(R.id.color_category_button);
        final LinearLayout numberCategoryButton = findViewById(R.id.number_category_button);
        final LinearLayout familyCategoryButton = findViewById(R.id.family_category_button);
        final LinearLayout phraseCategoryButton = findViewById(R.id.phrase_category_button);

        // Color category button
        colorCategoryButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(MainActivity.this, MiwokColor.class));
            }
        });


        // Number category button
        numberCategoryButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(MainActivity.this, MiwokNumber.class));
            }
        });


        // Family category button
        familyCategoryButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(MainActivity.this, MiwokFamily.class));
            }
        });

        // Phrase category button
        phraseCategoryButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(MainActivity.this, MiwokPhrase.class));
            }
        });
    }
}
