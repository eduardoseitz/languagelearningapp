package com.eduardoseitz.languagelearningapp;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MiwokFamily extends AppCompatActivity {

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miwok_family);

        // Create wordList
        final ArrayList<Word> wordList = new ArrayList<Word>();

        // Add list elements
        wordList.add(new Word( "әpә", "Father", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_father));
        wordList.add(new Word( "әṭa", "Mother", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_mother));
        wordList.add(new Word("Angsi", "Son", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_son));
        wordList.add(new Word("Tune", "Daughter", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_daughter));
        wordList.add(new Word("Taachi", "Older brother", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_older_brother));
        wordList.add(new Word("Chalitti ", "Younger brother", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_younger_brother));
        wordList.add(new Word("Teṭe", "Older sister", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_older_sister));
        wordList.add(new Word("Kolliti", "Younger sister", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_younger_sister));
        wordList.add(new Word("Ama", "Grandmother", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_grandmother));
        wordList.add(new Word("Paapa", "Grandfather", R.drawable.account_multiple_white, R.color.category_family, R.raw.family_grandfather));

        // Recycle list items with a list view
        WordArrayAdapter wordArrayAdapter = new WordArrayAdapter(this, wordList);

        ListView listView = findViewById(R.id.word_list_view);
        listView.setAdapter(wordArrayAdapter);

        // Play sound
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                // Create and setup the {@link MediaPlayer} for the audio resource associated
                // with the current word
                mediaPlayer = MediaPlayer.create(MiwokFamily.this, wordList.get(position).GetSound());

                // Start the audio file
                mediaPlayer.start();
            }
        });
    }
}
