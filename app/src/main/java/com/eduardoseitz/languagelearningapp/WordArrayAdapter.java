package com.eduardoseitz.languagelearningapp;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WordArrayAdapter extends ArrayAdapter<Word>
{
    int _currentColor;
    int _currentSound;

    // Custom constructor
    private static final String LOG_TAG = WordArrayAdapter.class.getSimpleName();

    public WordArrayAdapter(Activity context, ArrayList<Word> word)
    {
        super(context,0, word);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // Create a view
        View listItemView = convertView;

        if (listItemView == null)
        {
            // Link array adapter with layout item
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.word_item, parent, false);
        }

        // Get the link position
        Word currentWord = getItem(position);

        // Set up original word
        TextView originalText = listItemView.findViewById(R.id.original_word_text_view);
        originalText.setText(currentWord.GetOriginal());

        // Set up translated word
        TextView translatedText = listItemView.findViewById(R.id.translated_word_text_view);
        translatedText.setText(currentWord.GetTranslation());

        // Set up word icon
        ImageView wordIcon = listItemView.findViewById(R.id.word_icon_image_view);
        wordIcon.setImageResource(currentWord.GetIcon());

        // Set up word color
        _currentColor = currentWord.GetColor();
        LinearLayout wordLayout = listItemView.findViewById(R.id.word_item_linear_layout);
        wordLayout.setBackgroundColor(ContextCompat.getColor(getContext(), currentWord.GetColor()));

        // Set up word sound
        _currentSound = currentWord.GetSound();

        // Return the created view
        return listItemView;
    }
}
