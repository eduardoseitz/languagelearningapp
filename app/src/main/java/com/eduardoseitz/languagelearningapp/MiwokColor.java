package com.eduardoseitz.languagelearningapp;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MiwokColor extends AppCompatActivity {

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miwok_color);

        // Create wordList
        final ArrayList<Word> wordList = new ArrayList<Word>();

        // Add list elements
        wordList.add(new Word("Weṭeṭṭi", "Red", R.drawable.palette_white, R.color.red, R.raw.color_red));
        wordList.add(new Word("Chokokki", "Green", R.drawable.palette_white, R.color.green, R.raw.color_green));
        wordList.add(new Word("akaakki","Brown",  R.drawable.palette_white, R.color.brown, R.raw.color_brown));
        wordList.add(new Word("opoppi","Gray",  R.drawable.palette_white, R.color.gray, R.raw.color_gray));
        wordList.add(new Word("kululli","Black",  R.drawable.palette_white, R.color.black, R.raw.color_black));
        wordList.add(new Word("kelelli", "White", R.drawable.palette_white, R.color.white, R.raw.color_white));
        wordList.add(new Word("ṭopiisә", "Dusty Yellow", R.drawable.palette_white, R.color.dusty_yellow, R.raw.color_dusty_yellow));
        wordList.add(new Word("chiwiiṭә", "Mustard Yellow", R.drawable.palette_white, R.color.mustard_yellow, R.raw.color_mustard_yellow));

        // Recycle list items with a list view
        final WordArrayAdapter wordArrayAdapter = new WordArrayAdapter(this, wordList);

        ListView listView = findViewById(R.id.word_list_view);
        listView.setAdapter(wordArrayAdapter);

        // Play sound
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                // Create and setup the {@link MediaPlayer} for the audio resource associated
                // with the current word
                mediaPlayer = MediaPlayer.create(MiwokColor.this, wordList.get(position).GetSound());

                // Start the audio file
                mediaPlayer.start();
            }
        });
    }
}
